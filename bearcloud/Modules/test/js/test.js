﻿$(function () {
    Init();
    GetFileList();
})

var data, filedata;
function Init() {
    fileInit();
    $(".Previous").click(function () { pageturn("Previous") });
    $(".Next").click(function () { pageturn("Next") });
}

function fileInit() {
    $("#inputfile").fileinput({
        language: "zh",
        uploadUrl: "UPService.ashx",
        //allowedFileExtensions: ['xls', 'xlsx', 'doc', 'docx'],
        allowedFileExtensions: [],
        initialPreviewCount: 5,
        initialPreview: [

        ],
        initialPreviewAsData: true,
        initialPreviewConfig: [
            //{ caption: "Moon.jpg", size: 930321, width: "120px", key: 1 },
            //{ caption: "Earth.jpg", size: 1218822, width: "120px", key: 2 }
        ],
        maxFileCount: 5,
        deleteUrl: "/site/file-delete",    //删除操作的URL地址
        overwriteInitial: true,           //不允许覆盖初始的预览，所以添加文件时不会覆盖
        maxFileSize: 102400,                //文件最大不超过100KB
        uploadExtraData: function (previewId, index) {
            return { "FUNC": "ImportFile" };
        },
        initialCaption: ""       //默认名称
    }).on("fileuploaded", function (event, data) {
        filedata = data;
        //异步上传后返回结果处理
        //后台一定要返回json对象,空的也行。否则会出现提示警告。
        //返回对象的同时会显示进度条，可以根据返回值进行一些功能扩展
        if (data.response.data.length > 0) {
            $.ajax({
                url: "UPService.ashx",
                type: "post",
                dataType: "json",
                data: { "FUNC": "InsertUPLog", "FileName": data.response.data[0].caption },
                success: function (result) {
                    GetFileList();
                },
                error: function (result) {
                    console.log(result);
                    //$("#alertP").html("数据插入异常!");
                    //$("#alertModal").modal('toggle');
                }
            });
        }
    });
}


function GetFileList() {
    var par = {
        "FUNC": "GETFileList"
    }
    $.dataservice(par, function (returnValue) {
        var tbody = "";
        data = returnValue;
        if (returnValue.length >= 1) {
            pagebreak(returnValue, 1);
        } else {
            tbody = "<th>此查询条件下没有数据</th>";
            $("#ModalaccessoryTbody").html(tbody);
        }
    });
}

//翻页
function pageturn(type) {
    var pagenum = $("#pagenum").text();
    switch (type) {
        case "Previous":
            pagebreak(data, parseInt(pagenum) - 1)
            break;
        case "Next":
            pagebreak(data, parseInt(pagenum) + 1)
            break;
    }
}

function pagebreak(returnValue, pagenum) {
    var totalpage = Math.ceil(returnValue.length / 10) //除法取整
    if (pagenum == 0) { }
    else if (pagenum <= totalpage) {
        var star = (parseInt(pagenum) - 1) * 10 + 1;
        var end = returnValue.length < parseInt(pagenum) * 10 ? returnValue.length : parseInt(pagenum) * 10;
        var tbody = "";
        for (var i = parseInt(star) - 1; i < end; i++) {
            tbody += "<tr>" +
                    "<th scope='row'>" + parseInt(i + 1) + "</th>" +
                    //"<th>" + returnValue[i].EntName + "</th>" +
                    "<th style=display:none>" + returnValue[i].ID + "</th>" +
                    "<th>" + returnValue[i].FileName + "</th>" +
                    "<th>" + returnValue[i].submitTime.substring(0, 10) + "</th>" +
                    "<th>" +
                        "<button type='button' class='btn btn-xs btn-default' data-toggle='modal' style='background:#a7d7f6;' value='" + returnValue[i].ID + "' onclick=downLoadFile('" + returnValue[i].ID + "','" + returnValue[i].FileName + "') >下载</button>" +
                        "<button type='button' class='btn btn-xs btn-default' data-toggle='modal' style='background:#a7d7f6;'  value='" + returnValue[i].ID + "' onclick=DeleteFile('" + returnValue[i].ID + "','" + returnValue[i].FileName + "')>删除</button>" +
            "</th>" +
        "</tr>"
        };
        if (pagenum == 1) {
            $(".Previous").attr("disabled", "disabled")
            $(".Previous").attr("style", "background-color: #d8dad9;")
        } else if (pagenum == totalpage) {
            $(".Next").attr("disabled", "disabled")
            $(".Next").attr("style", "background-color: #d8dad9;")
        } else {
            $(".Previous").removeAttr("disabled");
            $(".Previous").attr("style", "");
            $(".Next").removeAttr("disabled");
            $(".Next").attr("style", "");
        }
        $("#pagenum").html(pagenum);
        $("#totalpagenum").html(totalpage);
        $("#ModalaccessoryTbody").html(tbody);
    }
}

//下载文件
function downLoadFile(ID,Name) {
    if (Name.indexOf('.txt') > -1) {
        $.get(downConfig.testdownUrl + Name, function (data, status) {
            if (status == "success") {
                download("data:text/plain," + data + "", Name, "text/plain")
            }
        });
    } else {
        window.location = downConfig.testdownUrl + Name;
    }
}

function DeleteFile(ID, Name) {
    var par = {
        "FUNC": "DeleteFile",
        "ID":ID
    }
    if (confirm("确定删除[" + Name + "]这个文件吗?")) {
        $.dataservice(par, function (returnValue) {
            if (returnValue == "success") {
                $("#alertP").html("删除文件["+Name+"]成功!");
                $("#alertModal").modal('toggle');
                GetFileList();
            }
        });
    }
}