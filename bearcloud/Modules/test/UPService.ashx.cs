﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace bearcloud.Modules.test
{
    /// <summary>
    /// UPService 的摘要说明
    /// </summary>
    public class UPService : IHttpHandler
    {
        public bool IsReusable
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            System.Collections.Specialized.NameValueCollection coll = context.Request.Form;
            String[] requestItem = coll.AllKeys;
            Dictionary<string, string> sArray = new Dictionary<string, string>();
            int i = 0;
            for (i = 0; i < requestItem.Length; i++)
            {
                sArray.Add(requestItem[i], context.Request.Form[requestItem[i]]);
            }
            if (sArray["FUNC"] == "ImportFile")
            {
                //string FileName = sArray["FileName"];
                //string ProID = sArray["ProID"];
                //string format = Path.GetExtension(FileName);
                context.Response.Write(ImportFile()); 
            }
            else if(sArray["FUNC"] == "GETFileList") {
                context.Response.Write(GETFileList()); ;
            }
            else if (sArray["FUNC"] == "InsertUPLog")
            {
                string FileName = sArray["FileName"];
                context.Response.Write(InsertUPLog(FileName)); ;
            }
            else if (sArray["FUNC"] == "DeleteFile")
            {
                string ID = sArray["ID"];
                context.Response.Write(DeleteFile(ID)); ;
            }
        }

        private JObject DeleteFile(string ID)
        {
            var sql = @"update [FileLog] set statu='1' where ID='"+ID+"'";
            string connStr = ConfigurationManager.ConnectionStrings["DefaultDB"].ConnectionString;
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connStr;
            conn.Open();
            SqlCommand cmd = new SqlCommand(); cmd.Connection = conn;
            cmd.CommandText = sql; cmd.CommandType = CommandType.Text;
            cmd.CommandTimeout = 300;
            try
            {
                cmd.ExecuteNonQuery();
                string result = "{\"returnValue\":\"success\"}";
                return (JObject)JsonConvert.DeserializeObject(result);
                conn.Close();
            }
            catch (Exception e)
            {
                string result = "{\"returnValue\":\"error:" + e.Message + "\"}";
                return (JObject)JsonConvert.DeserializeObject(result);
                conn.Close();
            }
        }

        private string GETFileList()
        {
            string ID = System.Guid.NewGuid().ToString();
            var sql = @"SELECT [ID],[FileName],convert(varchar,[submitTime],120) as submitTime  FROM [BearCloud].[dbo].[FileLog] where statu=0 ";
            string connStr = ConfigurationManager.ConnectionStrings["DefaultDB"].ConnectionString;
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connStr;
            conn.Open();
            SqlCommand cmd = new SqlCommand(); cmd.Connection = conn;
            cmd.CommandText = sql; cmd.CommandType = CommandType.Text;
            cmd.CommandTimeout = 300;
            try
            {
                SqlDataReader dr=cmd.ExecuteReader();
                string json = ToJson(dr);
                return json;
                conn.Close();
            } 
            catch (Exception e)
            {
                string result = "{\"returnValue\":\"error:" + e.Message + "\"}";
                return result;
                conn.Close();
            }
        }

        private JObject InsertUPLog(string FileName)
        {
            string ID = System.Guid.NewGuid().ToString();
            string FileDir = System.AppDomain.CurrentDomain.BaseDirectory + @"\Modules\test\uploadfile\" + FileName;
            var sql = @"insert into [FileLog]([ID],[FileName],[dir],[submitTime],statu) values('"+ ID + "','"+ FileName + "','"+ FileDir + "',GETDATE(),'0')";
            string connStr = ConfigurationManager.ConnectionStrings["DefaultDB"].ConnectionString;
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connStr;
            conn.Open();
            SqlCommand cmd = new SqlCommand(); cmd.Connection = conn;
            cmd.CommandText = sql; cmd.CommandType = CommandType.Text;
            cmd.CommandTimeout = 300;
            try
            {
                cmd.ExecuteNonQuery();
                string result = "{\"returnValue\":\"success\"}";
                return (JObject)JsonConvert.DeserializeObject(result);
                conn.Close();
            }
            catch (Exception e)
            {
                string result = "{\"returnValue\":\"error:" + e.Message + "\"}";
                return (JObject)JsonConvert.DeserializeObject(result);
                conn.Close();
            }
        }

        public JObject ImportFile()
        {
            var oStream = HttpContext.Current.Request.Files;
            string jsonText = "{data:[";
            for (int i = 0; i < oStream.Count; i++)
            {
                HttpPostedFile PostedFile = oStream[i];
                if (PostedFile.ContentLength > 0)
                {
                    string FileName = PostedFile.FileName;
                    string strPath = System.AppDomain.CurrentDomain.BaseDirectory + @"\Modules\test\uploadfile\"; //上载路径
                    PostedFile.SaveAs(strPath + FileName);
                    jsonText += "{\"caption\":\"" + FileName + "\",\"size\":\"" + PostedFile.ContentLength + "\",\"width\":\"120px\",\"key\":\"" + i + 1 + "\"},";
                }
            }
            jsonText += "]}";
            JObject jo = (JObject)JsonConvert.DeserializeObject(jsonText);
            return jo;
        }

        /// <summary> 
        /// DataReader转换为Json 
        /// </summary> 
        /// <param name="dataReader">DataReader对象</param> 
        /// <returns>Json字符串</returns> 
        public static string ToJson(SqlDataReader dataReader)
        {
            StringBuilder jsonString = new StringBuilder();
            jsonString.Append("[");
            while (dataReader.Read())
            {
                jsonString.Append("{");
                for (int i = 0; i < dataReader.FieldCount; i++)
                {
                    Type type = dataReader.GetFieldType(i);
                    string strKey = dataReader.GetName(i);
                    string strValue = dataReader[i].ToString();
                    jsonString.Append("\"" + strKey + "\":");
                    strValue = String.Format(strValue, type);
                    if (i < dataReader.FieldCount - 1)
                    {
                        jsonString.Append("\"" + strValue + "\",");
                    }
                    else
                    {
                        jsonString.Append("\"" + strValue + "\"");
                    }
                }
                jsonString.Append("},");
            }
            dataReader.Close();
            jsonString.Remove(jsonString.Length - 1, 1);
            jsonString.Append("]");
            return jsonString.ToString();
        }
    }
}