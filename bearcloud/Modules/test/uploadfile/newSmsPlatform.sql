if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('newSmsPlatform_project') and o.name = 'FK_NEWSMSPL_REFERENCE_NEWSMSPL')
alter table newSmsPlatform_project
   drop constraint FK_NEWSMSPL_REFERENCE_NEWSMSPL
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('newSmsPlatform_project') and o.name = 'FK_NEWSMSPL_REFERENCE_NEWSMSPL')
alter table newSmsPlatform_project
   drop constraint FK_NEWSMSPL_REFERENCE_NEWSMSPL
go

if exists (select 1
            from  sysobjects
           where  id = object_id('newSmsPlatform_project')
            and   type = 'U')
   drop table newSmsPlatform_project
go

/*==============================================================*/
/* Table: newSmsPlatform_project                                */
/*==============================================================*/
create table newSmsPlatform_project (
   ID                   int                  identity,
   projectName          varchar(64)          null,
   projectCode          varchar(32)          null,
   projectType          varchar(8)           null,
   projectStatu         varchar(8)           null,
   functionary          varchar(12)          null,
   submitTime           datetime             null,
   proStartTime         datetime             null,
   proEndTime           datetime             null,
   projectDescribe      varchar(1024)        null,
   MessageBatch         int                  null,
   SendCount            int                  null,
   PackType             varchar(8)           null,
   accessoryID          int                  null
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('newSmsPlatform_project') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'newSmsPlatform_project' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '项目总表', 
   'user', @CurrentUser, 'table', 'newSmsPlatform_project'
go


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('newSmsPlatform_Accessory') and o.name = 'FK_NEWSMSPL_REFERENCE_NEWSMSPL')
alter table newSmsPlatform_Accessory
   drop constraint FK_NEWSMSPL_REFERENCE_NEWSMSPL
go

if exists (select 1
            from  sysobjects
           where  id = object_id('newSmsPlatform_Accessory')
            and   type = 'U')
   drop table newSmsPlatform_Accessory
go

/*==============================================================*/
/* Table: newSmsPlatform_Accessory                              */
/*==============================================================*/
create table newSmsPlatform_Accessory (
   accessoryID          int                  identity,
   accessoryContent     varchar(256)         null,
   submitTime           datetime             null,
   projectID            int                  null
)
go


if exists (select 1
            from  sysobjects
           where  id = object_id('newSmsPlatform_proChange')
            and   type = 'U')
   drop table newSmsPlatform_proChange
go

/*==============================================================*/
/* Table: newSmsPlatform_proChange                              */
/*==============================================================*/
create table newSmsPlatform_proChange (
   ID                   int                  identity,
   ChangeConten         varchar(1024)        null,
   ProID                int                  null,
   updateTime           datetime             null,
   updater              varchar(8)           null
)
go

if exists (select 1
            from  sysobjects
           where  id = object_id('newSmsPlatform_audit')
            and   type = 'U')
   drop table newSmsPlatform_audit
go

/*==============================================================*/
/* Table: newSmsPlatform_audit                                  */
/*==============================================================*/
create table newSmsPlatform_audit (
   ID                   int                  identity,
   auditContent         varchar(1024)        null,
   proID                int                  null,
   auditer              varchar(8)           null,
   auditTime            datetime             null
)
go
